const input = 'index.js'
const sourcemap = true

export default [{
    input,
    output: {
        file: 'dist/real-value-infrastructure-base.mjs',
        format: 'es',
        sourcemap
    }
}, {
    input,
    output: {
        file: 'dist/real-value-infrastructure-base.js',
        format: 'cjs',
        sourcemap
    }
},
{
    input,
    output: {
        file: 'dist/real-value-infrastructure-base.umd.js',
        format: 'umd',
        name: 'real-value-infrastructure-base',
        sourcemap
    }
}]
