import { describe } from 'riteway'
import Infrastructure from '../index.js'
import fs from 'fs'
import { ReadableAdapter } from 'real-value-stream-adapter'
const debug = require('debug')('infrastructure.test')

let event = 'value'

let infrastructure = Infrastructure()

// describe('Pipe', async (assert) => {
//     let piped = null
//     infrastructure.pipe({ event: (x) => {
//       piped = x
//     } }, event)

//     assert({
//       given: 'pipe some event to a stream',
//       should: 'the stream event should be called',
//       actual: piped,
//       expected: 'value'
//     })
// })

describe('Tap', async (assert) => {
  let tapCalled = false
  let downstreamCalled = false
  infrastructure.tap(async x => { tapCalled = true }, 'somevalue', x => { downstreamCalled = true }, () => {
    assert({
      given: 'tap a stream with a function',
      should: 'the function should be called and the input value passed downstream after the function returns',
      actual: `${tapCalled} ${downstreamCalled}`,
      expected: 'true true'
    })
  })
})

describe('Take', async (assert) => {
  let downstreamCalled = false
  let taker = infrastructure.take(1)
  taker('somevalue', x => { downstreamCalled = true }, () => {
    assert({
      given: 'taker when below threshold',
      should: 'the input value should be passed downstream',
      actual: `${downstreamCalled}`,
      expected: 'true'
    })
  })
  let downstreamCalled2 = false
  taker('nextvalue', x => { downstreamCalled2 = true }, () => {
    assert({
      given: 'taker when below threshold',
      should: 'the input value should not be passed downstream',
      actual: `${downstreamCalled2}`,
      expected: 'false'
    })
  })
})

describe('Filter Include', async (assert) => {
  let downstreamCalled = false
  infrastructure.filter(x => x === 'somevalue', true/* include */, 'somevalue', x => { downstreamCalled = true }, () => {
    assert({
      given: 'filter a stream with a function',
      should: 'filter includes value',
      actual: `${downstreamCalled}`,
      expected: 'true'
    })
  })
})
describe('Filter Exclude', async (assert) => {
  let downstreamCalled = false
  infrastructure.filter(x => x === 'somevalue', false/* exclude */, 'somevalue', x => { downstreamCalled = true }, () => {
    assert({
      given: 'filter a stream with a function',
      should: 'filter includes value',
      actual: `${downstreamCalled}`,
      expected: 'false'
    })
  })
})

describe('Table', async (assert) => {
  let downstreamCalled = false
  let { get, process } = infrastructure.table({
      key: x => x.key,
      reducer: (h, n) => h === undefined ? n.value : h + n.value
  })
  process({ key: 'a', value: 1 })
  process({ key: 'a', value: 1 })
  process({ key: 'b', value: 2 })
  process({ key: 'b', value: 3 })
  process({ key: 'a', value: 1 }, x => { downstreamCalled = true }, () => {
    assert({
      given: 'table a stream with a function',
      should: 'the reduction should occur and downstream called for changes',
      actual: `${downstreamCalled} ${get('a')} ${get('b')}`,
      expected: 'true 3 5'
    })
  })
})

describe('BUffer', async (assert) => {
  let downstreamCount = 0
  let bufferer = infrastructure.buffer(1)
  bufferer('Test', x => { downstreamCount++ }, () => {
    assert({
      given: 'buffer(1) a stream',
      should: 'not pass first value downstream',
      actual: `${downstreamCount}`,
      expected: '0'
    })
  })
  let downstreamCalled2 = false
  bufferer('Test2', x => { downstreamCount++ }, () => {
    assert({
      given: 'buffer(1) a stream',
      should: 'should pass second(and 1st) value downstream',
      actual: `${downstreamCount}`,
      expected: '2'
    })
  })
})

describe('Batch', async (assert) => {
  let downstreamCount = 0
  let numberOfEvents = 0
  let closedownCalled = false

  let { batcher } = infrastructure.batch(x => x.type, x => x, 20, 2, () => {
    closedownCalled = true
    assert({
      given: 'batch(type,100,2) a stream',
      should: 'should emit separate batches when there are 2 different types',
      actual: `${downstreamCount} ${numberOfEvents} ${closedownCalled}`,
      expected: '4 6 true'
    })
  })

  let reducer = async x => { downstreamCount++; numberOfEvents += x.batch.length }
  batcher({ type: 1, value: 1 }, reducer, async () => {})
  batcher({ type: 2, value: 2 }, reducer, async () => {})
  batcher({ type: 1, value: 3 }, reducer, async () => {})
  batcher({ type: 2, value: 4 }, reducer, async () => {})
  batcher({ type: 1, value: 5 }, reducer, async () => {})
  batcher({ type: 2, value: 6 }, reducer, async () => {})
  })

describe('Join', async (assert) => {
  let scenarios = [
    { type: 'inner', preceding: false, following: true, flushCount: 0 },
    { type: 'left', preceding: false, following: true, flushCount: 1 },
    { type: 'right', preceding: false, following: true, flushCount: 1 },
    { type: 'outer', preceding: false, following: true, flushCount: 2 },
    { type: 'xor', preceding: false, following: false, flushCount: 2 }
  ]
  scenarios.forEach((scenario) => {
      let downstreamCalled = false
      let { join1, join2, flush } = infrastructure.join({ type: scenario.type })
      join1({ key: 1, value1: 1 }, x => {
        downstreamCalled = true
      }, () => {
        assert({
          given: 'join1 preceding join2',
          should: 'downstream not called',
          actual: downstreamCalled,
          expected: scenario.preceding
        })
      })
      let downstreamCalled2 = false
      join2({ key: 1, value2: 1 }, x => {
        downstreamCalled2 = true
      }, () => {
        assert({
          given: 'join2 following join1',
          should: 'downstream called',
          actual: downstreamCalled2,
          expected: scenario.following
        })
      })
      join1({ key: 2, value1: 2 }, x => {}, () => {})
      join2({ key: 3, value1: 3 }, x => {}, () => {})
      let flushed = []
      flush(x => { flushed.push(x) })
      assert({
        given: 'flushed',
        should: 'unflushed items should be pushed downstream',
        actual: `${flushed.length}`,
        expected: `${scenario.flushCount}`
      })
    })
  })

  describe('Join with different key function', async (assert) => {
    let { join1, join2, flush } = infrastructure.join({ type: 'left', keyFn2: x => x.key2 })

    let callback = () => {}
    let downstream = () => {}
    join1({ key: 1, value1: 1 }, downstream, callback)

    let secondKeyAndMatchingValueShouldJoin = false
    join2({ key2: 1, value1: 1 }, () => {
      secondKeyAndMatchingValueShouldJoin = true
     }, callback)
     let firstKeyAndMatchingValueShouldNotJoin = true
    join2({ key: 1, value1: 2 }, () => {
      firstKeyAndMatchingValueShouldNotJoin = false
     }, callback)

     assert({
      given: 'a keyFn2 function',
      should: 'join correctly',
      actual: `${secondKeyAndMatchingValueShouldJoin} ${firstKeyAndMatchingValueShouldNotJoin}`,
      expected: `true true`
    })
  })

  describe('Combine', async (assert) => {
    let scenarios = [
      { source1: [1], source2: [10], flushCount: 1 },
      { source1: [1, 2], source2: [10], flushCount: 2 },
      { source1: [1, 2], source2: [10, 20], flushCount: 4 },
      { source1: [], source2: [1], flushCount: 0 }
    ]
    scenarios.forEach((scenario) => {
        let downstreamCalled = false
        let { combine1, combine2, flush } = infrastructure.combine({ combineFn: (x, y) => {
          // console.log(`${x}-${y}`)
          return x + y
} })
        let downstreamcount = 0
        scenario.source1.forEach(item => combine1(item, () => {}))
        scenario.source2.forEach(item => combine2(item, () => {}))
        flush((x) => {
          downstreamcount++
        }, () => {
          assert({
            given: 'combine',
            should: 'correct number of combinations',
            actual: downstreamcount,
            expected: scenario.flushCount
          })
        })
      })
    })

describe('ToFile', async (assert) => {
  let downstreamCalled = false
  let { eventHandler, flush } = infrastructure.toFile('./tmp/toFile.txt', { firstline: 'firstline', flags: 'create' })
  eventHandler('sometext', x => { downstreamCalled = true }, () => {})
  flush((numLines) => {
    assert({
      given: 'call to toFile',
      should: 'call write content',
      actual: `${numLines}`,
      expected: '1'
    })
  })
})

describe('ToVariable-csv', async (assert) => {
  let downstreamCalled = false
  let varname = 'foo'
  let map = {}
  let { eventHandler, flush } = infrastructure.toVariable(varname, map, { separator: '' })
  eventHandler('a,b,c\n', x => { downstreamCalled = true }, () => {})
  eventHandler('1,2,3\n', x => { downstreamCalled = true }, () => {})
  eventHandler('\n', x => { downstreamCalled = true }, () => {})
  flush(() => {
    assert({
      given: 'calls to toVariable',
      should: 'call content accumulate',
      actual: `${map[varname]}`,
      expected: 'a,b,c\n1,2,3\n'
    })
  })
})

describe('ToVariable-object', async (assert) => {
  let downstreamCalled = false
  let varname = 'foo'
  let map = {}
  let { eventHandler, flush } = infrastructure.toVariable(varname, map, {})
  eventHandler({}, x => { downstreamCalled = true }, () => {})
  eventHandler({}, x => { downstreamCalled = true }, () => {})
  flush(() => {
    assert({
      given: 'calls to toVariable',
      should: 'call content accumulate',
      actual: `${map[varname].length}`,
      expected: '2'
    })
  })
})

describe('AsCSV', async (assert) => {
  let downstreamCount = 0
  let countDownstream = x => { downstreamCount++ }
  let doNothingCallback = x => {}
  let { eventHandler, flush } = infrastructure.asCSV()
  eventHandler({ b: 2, a: '"1"', c: 3 }, countDownstream, doNothingCallback)
  eventHandler({ b: 200, a: 100, d: 400 }, countDownstream, doNothingCallback)
  eventHandler({ b: 20, a: 10, c: 30, d: 0 }, countDownstream, doNothingCallback)
  flush((numColumns, numLines) => {
    assert({
      given: 'call to toCSV',
      should: 'correct number of columns,lines and downstream calls',
      actual: `${numColumns} ${numLines} ${downstreamCount}`,
      expected: '4 3 4'
    })
  }, countDownstream/* downstream */)
})

// describe('ToAzure', async (assert) => {
//   let config = {
//     filename: 'Test.csv',
//     account: 'projectcontrolsstorage',
//     accountKey: process.env.ACCESS_KEY || '',
//     shareName: 'projectcontrols',
//     directoryName: `ReportingDataSet`
//   }
//   let { eventHandler, flush } = infrastructure.toAzure(config, { debugOnly: true })
//   eventHandler('a,b,c\n', x => {}, () => {})
//   eventHandler('1,2,3\n', x => {}, () => {})
//   flush((numColumns, numLines) => {
//     assert({
//       given: 'call to toAzure',
//       should: 'correct number of lines written',
//       actual: `1`,
//       expected: '1'
//     })
//   })
// })

describe('FromCSV-string', async (assert) => {
  let downstreamCalled = false
  let generator = infrastructure.fromCSV('"a","b"\n"1","2"\n"3","4"\n')
  let countrows = 0
  for await (const row of generator) {
    debug(row)
    countrows++
  }
  assert({
    given: 'a csv',
    should: 'fromCSV gets all rows',
    actual: `${countrows}`,
    expected: '2'
  })
})

describe('FromCSV-file', async (assert) => {
  let downstreamCalled = false
  let generator = infrastructure.fromCSV('./test/test.csv')
  let countrows = 0
  for await (const row of generator) {
    debug(row)
    countrows++
  }
  assert({
    given: 'a csv',
    should: 'fromCSV gets all rows',
    actual: `${countrows}`,
    expected: '3'
  })
})

describe('FromCSV-variable', async (assert) => {
  let downstreamCalled = false
  let generator = infrastructure.fromCSV('test.var', { variables: { 'test.var': 'a,b,c\n1,2,3\n4,5,6\n' } })
  let countrows = 0
  for await (const row of generator) {
    debug(row)
    countrows++
  }
  assert({
    given: 'a csv',
    should: 'fromCSV gets all rows',
    actual: `${countrows}`,
    expected: '2'
  })
})

describe('FromLines', async (assert) => {
  // let readable = fs.createReadStream('./test/test.csv')
  // let generator = infrastructure.fromLines(readable)
  let g = function * () {
    yield 'a,b,'
    yield 'c\r\n1,'
    yield '2,'
    yield '3\r\n4,5,6'
  }
  let readable = ReadableAdapter(g())
  let generator = infrastructure.fromLines(readable)

  let countrows = 0
  for await (const row of generator) {
    debug(row)
    countrows++
  }
  assert({
    given: 'a csv',
    should: 'fromLines gets all rows',
    actual: `${countrows}`,
    expected: '3'
  })
})
