# real-value-infrastructure-base Library     

## About

This library provides some based 'infrastructure' for the real-value-lang intepreter.
The term 'infrastrucutre' is used to convey that this code connects the interpreter to the operating system and to persistance and messaging services. 

## Install

```
npm install real-value-infrastructure-base
yarn install real-value-infrastructure-base
```

## How to use

This functionality is used by default in the real-value-lang interpreter.
As a base library the intention is that it can be augemented and used in place of this library.

Infrastructure is passed the real-lang-model instances as they are created.
```
 let amodel = Model(Infrastructure())
```

Consequently it is possible to create some infrastructure and then modify it.
```
let infrastructure = Infrastructure()
infrastructure.table= ()=>{....}
let amodel = Model(Infrastructure())
```

Here are the signatures for each infrastructure capability provided:
```
createQueue: function(){
},
createPipe: (downstream, options) => {
},
toFile: function (stream, filename, options) {
},
fromCSV: function (stream, file, options) {
},
fromXLSX: function (stream, file, options) {
},
fromArray: function (stream, streamdata) {
},
fromGenerator: function (stream, generator) {
},
log: function (stream, e) {
},
tap: function (stream, f, x) {
},
map: function (stream, f, x) {
},
split: function (stream, x) {
},
merge: function (stream, x) {
},
table: function (stream, key = x => x.key) {
},
join: function (stream, x, key) {
},
take: function (count) {
},
buffer: function (count) {
    //buffering involves capture the first set of records before releasing them individually 
},
batch: function (stream,delay,maxbatch) {
    //Support batching the events where events are batched into periods of delay and max batch size.
    //  1,2,3,,4,5,,6,, => ,,,[1,2,3],,,,[4,5],[6]
},
filter: function (stream, f, x) {
},
delay: async function (stream, x, delay) {
}
```
