const debug = require('debug')('infrastructure')
const { WritableAdapter, ReadableAdapter2 } = require('real-value-stream-adapter')
const { Readable } = require('stream')

let { compare } = require('./compare')

const waitFor = ms => new Promise(res => setTimeout(res, ms))

let fs = null
function appendwrite (x, filename) {
    if (fs) {
        fs.appendFile(filename, x, (err) => {
            if (err != null) {
                console.error(err)``
                throw err
            }
            })
    }
}
function createwrite (x, filename) {
    if (fs) {
        fs.writeFileSync(filename, x)
    }
}

module.exports = function () {
    try {
        fs = require('fs')
    } catch (err) {
        // doesnt work in the browser
    }
    return {
        createQueue: (options) => {
            let ArrayQueue = require('real-value-arrayqueue')
            return ArrayQueue(options)
        },

        createGeneratorFromChannel: (channel, options = {}) => {
            const { yieldTotal = -1 } = options
            let yieldCount = 0
            async function * generator () {
                while (yieldTotal < 0 || yieldTotal >= yieldCount) { // eslint-disable-line  no-unmodified-loop-condition
                    if (!channel.length()) {
                        await waitFor(1000)
                    } else {
                        let value = channel.dequeue()
                        debug(`yield ${value}`)
                        yieldCount = yieldCount + 1
                        yield value
                    }
                }
            }
            return generator
        },

        // createChannelServerFactory: (options={}) => {
        //     const { server} = options
        //     let { ChannelSocketIOServer } = require('real-value-channel-socketio')
        //     return ChannelSocketIOServer({server})
        // },
        // createChannelClientFactory: (options={}) => {
        //     const { url} = options
        //     let { ChannelSocketIOClient } = require('real-value-channel-socketio')
        //     return ChannelSocketIOClient({url})
        // },
        createPipe: (downstream, options) => {
            let { yieldTotal = -1 } = options
            let buffer = []
            const waitFor = ms => new Promise(res => setTimeout(res, ms))
            let yieldCount = 0
            async function * generator () {
                while (yieldCount < yieldTotal) {
                    if (!buffer.length) {
                        await waitFor(1000)
                    } else {
                        let value = buffer.pop()
                        debug(`pub yield ${value}`)
                        yieldCount++
                        yield value
                    }
                }
            }
            let pipe = (value) => {
                debug(`pub pushing ${value}`)
                buffer.push(value)
            }
            return {
                pipe,
                generator
            }
        },
        asCSV: function (options) {
            debug(`asCSV`)
            let headerWritten = false
            let sortedColumns = null
            let columns = {}
            function updateColumns (record) {
                Object.keys(record).forEach(col => {
                    columns[col] = true
                })
            }
            function header () {
                sortedColumns = Object.keys(columns).sort()
                return sortedColumns.map(x => `"${x}"`).join()
            }
            let lines = []
            let eventHandler = (x, downstream, callback) => {
                updateColumns(x)
                lines.push(x)
                callback()
            }
            let flush = (callback, downstream) => {
                downstream(header() + '\n')
                lines.forEach(line => {
                    let output = sortedColumns.map(col => {
                        let column = line[col]
                        if (column === undefined) {
                            return ''
                        } else if (typeof column === 'string') {
                            if (column.startsWith('"')) {
                                return `${line[col]}`
                            } else {
                                return `"${line[col]}"`
                            }
                        } else {
                            return `"${line[col]}"`
                        }
                    }).join() + '\n'
                    debug(output)
                    downstream(output)
                })
                if (callback) { callback(Object.keys(columns).length, lines.length) }
                lines = []
            }
            return {
                eventHandler,
                flush
            }
        },
        toStream: function (reference, options) {
            if (reference.endsWith('.var')) {
                let variables = options.variables
                return this.toVariable(reference, variables, options)
            } else {
                return this.toFile(reference, options)
            }
        },
        toVariable: function (varname, map, options) {
            debug(`toVariable : ${varname}`)
            let lines = []
            let { separator = '|' } = options
            let eventHandler = (x, downstream, callback) => {
                if (typeof x === 'object') {
                    lines.push(x)
                } else if (typeof x === 'string' && x.trim().length > 0) {
                    lines.push(x)
                }
                downstream(x)
                callback()
            }
            let flush = (callback) => {
                if (typeof lines[0] === 'string') {
                    map[varname] = lines.join(separator)
                } else {
                    map[varname] = lines
                }
                callback()
            }
            return {
                eventHandler,
                flush
            }
        },
        toFile: function (thefilename, options) {
            debug(`toFile : ${thefilename}`)
            let filename = thefilename
            let { debugOnly = false, firstline = null, flags = 'create', format = null } = options
            let lines = []
            let eventHandler = (x, downstream, callback) => {
                let output = x
                if (format) {
                    output = format(x)
                }
                lines.push(output)
                downstream(x)
                callback()
            }
            let flush = (callback) => {
                var writeStream = fs.createWriteStream(filename, { flags: 'w', emitClose: true })
                debug('write stream opened')
                writeStream.on('close', () => {
                    debug('write stream closed')
                    if (callback) { callback(lines.length) }
                })
                if (firstline) {
                    writeStream.write(firstline)
                    writeStream.write('\n')
                }
                lines.forEach(line => {
                    writeStream.write(line)
                })
                writeStream.close()
            }
            return {
                eventHandler,
                flush
            }
        },
        // toAzure: function (config, options) {
        //     debug(`toAzure : ${config.filename}`)
        //     let { debugOnly = false } = options
        //     let storage = null
        //     try {
        //         storage = require('@azure/storage-file')
        //     } catch (err) {
        //         if (err.code === 'MODULE_NOT_FOUND') {
        //             throw new Error('Please install @azure/storage-file package manually')
        //         } else { throw err }
        //     }
        //     const {
        //         Aborter,
        //         StorageURL,
        //         ServiceURL,
        //         ShareURL,
        //         DirectoryURL,
        //         FileURL,
        //         SharedKeyCredential,
        //         AnonymousCredential,
        //         uploadStreamToAzureFile
        //       } = storage

        //     const account = config.account
        //     const accountKey = config.accountKey
        //     const sharedKeyCredential = new SharedKeyCredential(account, accountKey)
        //     const anonymousCredential = new AnonymousCredential()
        //     const pipeline = StorageURL.newPipeline(sharedKeyCredential)
        //     const serviceURL = new ServiceURL(
        //       `https://${account}.file.core.windows.net`,
        //       pipeline
        //     )
        //     const shareName = config.shareName
        //     const shareURL = ShareURL.fromServiceURL(serviceURL, shareName)
        //     const directoryName = config.directoryName
        //     const directoryURL = DirectoryURL.fromShareURL(shareURL, directoryName)
        //     const fileName = config.filename
        //     const fileURL = FileURL.fromDirectoryURL(directoryURL, fileName)
        //     let lines = []
        //     let length = 0
        //     let eventHandler = (x, downstream, callback) => {
        //         let tidy = x.replace(/[^\x00-\x7F]/g, '') // eslint-disable-line no-control-regex
        //         lines.push(tidy)
        //         length += tidy.length
        //         downstream(tidy)
        //         callback()
        //     }
        //     let flush = async (callback) => {
        //         function * generator () {
        //             for (var i = 0; i < lines.length; i++) {
        //                 yield lines[i]
        //             }
        //         }
        //         if (!debugOnly) {
        //             await uploadStreamToAzureFile(
        //                 Aborter.timeout(30 * 60 * 60 * 1000), // Abort uploading with timeout in 30mins
        //                 Readable.from(generator()),
        //                 length,
        //                 fileURL,
        //                 4 * 1024 * 1024,
        //                 20,
        //                 {
        //                   // progress: ev => console.log(ev)
        //                 }
        //               )
        //         }
        //         callback()
        //     }
        //     return {
        //         eventHandler,
        //         flush
        //     }
        // },
        fromLines: function (readable, options = {}) {
            debug(`fromLines`)
            let { highWaterMark = 10000 } = options
            let linestream = null
            linestream = readable
            // if (reference.endsWith('.var')) {
            //     let variables = options.variables
            //     linestream = new Readable()
            //     linestream.push(variables[reference].trim())
            //     linestream.push(null)
            // } else {

            // }
            async function * generator () {
                let buffer = []
                let done = false
                let remainder = ''
                let add = (x) => {
                    let list = x.toString().split(/\r?\n/)
                    debug(list)
                    list.forEach((l, i, total) => {
                        debug(`${l} ${i} ${total.length}`)
                        let next = remainder
                        next = `${remainder}${l}`
                        if (i < (total.length - 1)) {
                            debug(`Push ${next}`)
                            buffer.push(next)
                            remainder = ''
                        } else {
                            debug(`Remainder ${next}`)
                            remainder = next
                        }
                    })
                    return buffer.length
                }
                let onDone = () => {
                    if (remainder.length > 0) {
                        debug(`Push ${remainder}`)
                        buffer.push(remainder)
                    }
                    done = true
                }

                let writableAdapter = WritableAdapter({
                    delay: 100,
                    highWaterMark,
                    add,
                    done: onDone
                })

                linestream.pipe(writableAdapter.writable)

                while (!done || buffer.length > 0) { // eslint-disable-line no-unmodified-loop-condition
                    if (buffer.length > 0) {
                        let row = buffer.shift()
                        yield row
                    } else if (!done) {
                        await new Promise((resolve) => setTimeout(() => resolve(), 100))
                    }
                }
            }
            return generator()
        },
        fromCSV: function (reference, options = {}) {

            const csv = require('csv-parser')
            
            debug(`fromCSV ${reference}`)
            let { highWaterMark = 10000 } = options
            let csvstream = null
            if (reference.endsWith('.var')) {
                let variables = options.variables
                csvstream = new Readable()
                csvstream.push(variables[reference].trim())
                csvstream.push(null)
            } else if (reference.endsWith('.csv')) {
                csvstream = fs.createReadStream(reference)
            } else {
                csvstream = new Readable()
                csvstream.push(reference.trim())
                csvstream.push(null)
            }
            async function * generator () {
                let buffer = []
                let done = false
                let add = (x) => {
                    // console.log(x)
                    buffer.push(x)
                    return buffer.length
                }
                let onDone = () => {
                    done = true
                }

                let writableAdapter = WritableAdapter({
                    delay: 100,
                    highWaterMark,
                    add,
                    done: onDone
                })

                csvstream
                    .pipe(csv())
                    .pipe(writableAdapter.writable)

                while (!done || buffer.length > 0) { // eslint-disable-line no-unmodified-loop-condition
                    if (buffer.length > 0) {
                        let row = buffer.shift()
                        yield row
                    } else if (!done) {
                        await new Promise((resolve) => setTimeout(() => resolve(), 100))
                    }
                }
            }
            return generator()
        },
        // fromXLSX: function (stream, file, options) {
        //     let { fromXLSX } = require('real-value-from-xlsx')
        //     debug(`fromXLSX ${file}`)
        //     let { generator } = fromXLSX(file)
        //     return generator()
        // },
        fromArray: function (stream, streamdata) {
            debug(`fromArray ${streamdata.length}`)
            function * generator () {
                for (var i = 0; i < streamdata.length; i++) {
                    debug(`fromArray yield ${JSON.stringify(streamdata[i])}`)
                    yield streamdata[i]
                }
            }
            return generator()
        },
        fromGenerator: function (stream, generator) {
            debug('fromGenerator')
            return generator()
        },
        /*
         * Returns a producer function and a Readable that generates events when the producer function is invoked
         */
        fromCallback: function () {
            debug('fromCallback')
            let { producer, readable } = ReadableAdapter2()
            return { producer, readable }
        },
        log: function (e) {
            console.log(e)
        },
        tap: async function (f, x, downstream, callback) {
            debug(`tap ${x}`)
            downstream(x)
            try {
                await f(x)
            } catch (err) {
                console.log(err)
                process.exit(1)
            } finally {
                callback()
            }
        },
        map: async function (f, x, downstream, callback) {
            debug(`map ${x}`)
            let mapped = await f(x)
            downstream(mapped)
            callback()
        },
        split: async function (x, downstream, callback) {
            debug(`split ${x}`)
            downstream(x)
            callback()
        },
        merge: function (stream, x) {
            debug(`merge ${x}`)
        },
        table: function (options) {
            let {
                key = x => x.key,
                reducer = (h, n) => n
            } = options
            debug(`table`)
            let tableData = {}
            let get = x => tableData[x]
            let process = (x, downstream = () => {}, callback = () => {}) => {
                let k = key(x)
                debug(`table event ${JSON.stringify(x, null, '')}`)
                let originalValue = tableData[k]
                let newValue = reducer(originalValue, x)
                debug(`NewValue: ${newValue}  OrigValue:${originalValue}`)
                let updated = originalValue ? !compare(originalValue, newValue) : true
                debug(`table updated ${updated}`)
                tableData[k] = newValue
                if (updated) { downstream(newValue) }
                callback()
            }
            return { get, process }
        },
        join: function (options = {}) {
            let {
                    keyFn = x => x.key,
                    keyFn2 = null,
                    joinFn = (x, y) => Object.assign(Object.assign({}, x), y),
                    type = 'outer'
            } = options
            const JOIN_TYPE_XOR = type === 'xor'
            const JOIN_TYPE_OUTER = type === 'outer'
            const JOIN_TYPE_INNER = type === 'inner'
            const JOIN_TYPE_LEFT = type === 'left'
            const JOIN_TYPE_RIGHT = type === 'right'
            let unmatchedData = [{}, {}]
            let matchedData = [{}, {}]

            if (!keyFn2) {
                debug('Using same key function')
                keyFn2 = keyFn
            }
            let joinFactory = (index, index2, keyFun) => (x, downstream, callback) => {
                let key = keyFun(x)
                if (unmatchedData[index2][key]) {
                    debug(`join${index} found unmatchedData for the key:${key}`)
                    unmatchedData[index2][key].forEach(m => {
                        if (JOIN_TYPE_XOR) {
                        } else {
                            let j = index2 === 0 ? joinFn(m, x) : joinFn(x, m)
                            if (j != null) { downstream(j) }
                        }
                    })
                    delete unmatchedData[index2][key]
                    delete unmatchedData[index][key]
                    matchedData[index][key] = x
                } else if (matchedData[index2][key]) {
                    debug(`join${index} found matchedData for key:${key}`)
                    if (JOIN_TYPE_XOR) {
                        return
                    } else if (JOIN_TYPE_LEFT && index2 === 0) {
                        return
                    } else {
                        let j = index2 === 0 ? joinFn(matchedData[index2][key], x) : joinFn(x, matchedData[index2][key])
                        if (j != null) { downstream(j) }
                    }
                } else {
                    debug(`join${index} no match for key:${key}`)
                    if (unmatchedData[index][key] === undefined) {
                        unmatchedData[index][key] = []
                    }
                    unmatchedData[index][key].push(x)
                    matchedData[index][key] = x
                }
                callback()
            }
            let flush = (downstream) => {
                // debug(`Flush0 ${Object.keys(matchedData[0]).length}`)
                debug(`Flush0 ${Object.keys(unmatchedData[0])}`)
                debug(`Flush1 ${Object.keys(unmatchedData[1])}`)
                if (JOIN_TYPE_OUTER || JOIN_TYPE_LEFT || JOIN_TYPE_XOR) {
                    Object.values(unmatchedData[0]).forEach(items => {
                        items.forEach(x => {
                            let j = joinFn(x, null)
                            if (j != null) { downstream(j) }
                        })
                    })
                }
                if (JOIN_TYPE_OUTER || JOIN_TYPE_RIGHT || JOIN_TYPE_XOR) {
                    Object.values(unmatchedData[1]).forEach(items => {
                        items.forEach(x => {
                            let j = joinFn(null, x)
                            if (j != null) { downstream(j) }
                        })
                    })
                }
            }
            return {
                join1: joinFactory(0, 1, keyFn),
                join2: joinFactory(1, 0, keyFn2),
                flush
            }
        },
        combine: function (options = {}) {
            let {
                    combineFn = (x, y) => Object.assign(Object.assign({}, x), y)
            } = options
            let combineData = [[], []]
            let combineFactory = (index) => (x, callback) => {
                debug(`Capture ${index}`)
                combineData[index].push(x)
                callback()
            }
            let flush = (downstream, callback) => {
                debug(`Flush0 ${combineData[0].length}`)
                debug(`Flush1 ${combineData[1].length}`)
                combineData[0].forEach(item1 => {
                    combineData[1].forEach(item2 => {
                        downstream(combineFn(item1, item2))
                    })
                })
                if (callback) { callback() }
            }
            return {
                combine1: combineFactory(0),
                combine2: combineFactory(1),
                flush
            }
        },
        take: function (count) {
            debug(`take ${count}`)
            let cnt = 0
            return (x, downstream, callback) => {
                cnt++
                if (count < 0 || cnt <= count) {
                    downstream(x)
                }
                callback()
            }
        },
        skip: function (count) {
            debug(`take ${count}`)
            let cnt = 0
            return (x, downstream, callback) => {
                cnt++
                if (cnt <= count) {
                    // skip
                } else {
                    downstream(x)
                }
                callback()
            }
        },
        buffer: function (count) {
            debug(`buffer ${count}`)
            let buffer = []
            let bufferingComplete = false
            return (x, downstream, callback) => {
                if (bufferingComplete) {
                    while (buffer.length > 0) {
                        let b = buffer.shift()
                        downstream(b)
                    }
                    downstream(x)
                } else {
                    buffer.push(x)
                }
                if (buffer.length >= count) {
                    bufferingComplete = true
                    debug(`bufferingComplete ${buffer.length}`)
                }
                callback()
            }
        },
        batch: function (typeFn, addToBatchFn, delay = 100, maxbatch = 1000, closedownCallback) {
            debug(`batch delay:${delay} maxbatch:${maxbatch}`)
            let StreamBatcher = require('stream-batcher')
            let { batchInProgress, streambatchFactory } = StreamBatcher(addToBatchFn, delay, maxbatch)
            let schedule = {}
            let progressTracking = []
            const batcher = async (x, downstream, callback) => {
                let type = typeFn(x)
                debug(type)
                if (schedule[type] === undefined) {
                    schedule[type] = streambatchFactory((batch) => {
                        debug(`Emitting event ${batch.length}`)
                        downstream({ type, batch })
                    }, () => {
                        let index = progressTracking.indexOf(type)
                        progressTracking.splice(index, 1)
                        debug(`Removed in progress: ${progressTracking}`)
                        if (progressTracking.length === 0) {
                            closedownCallback()
                        }
                    })
                }
                if (progressTracking.indexOf(type) === -1) { progressTracking.push(type) }
                debug(`Added in progress: ${progressTracking}`)
                schedule[type](x)
                callback()// need to call the callback so that node stream provides next value
            }
            return {
                batchInProgress,
                batcher
            }
        },
        filter: async function (f, filterToInclude = true, x, downstream, callback) {
            debug(`filter ${x}`)
            let matches = await f(x)
            if (matches) {
                if (filterToInclude) {
                    downstream(x)
                }
            } else {
                if (!filterToInclude) {
                    downstream(x)
                }
            }
            callback()
        },
        delay: async function (delay, x) {
            debug(`delay ${x}`)
            await new Promise((resolve) => setTimeout(() => resolve(), delay))
        }
    }
}
