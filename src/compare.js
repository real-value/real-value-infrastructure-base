
let compare = function (o1, o2) {
    var p
    if (o1 === undefined || o2 === undefined) { return false }

    if (typeof o1 === 'object' && typeof o2 === 'object') {
        for (p in o1) {
            if (o1.hasOwnProperty(p)) {
                if (o1[p] !== o2[p]) {
                    return false
                }
            }
        }
        for (p in o2) {
            if (o2.hasOwnProperty(p)) {
                if (o1[p] !== o2[p]) {
                    return false
                }
            }
        }
    } else {
        return o1 == o2 // eslint-disable-line eqeqeq
    }
    return true
}

module.exports = {
    compare
}
